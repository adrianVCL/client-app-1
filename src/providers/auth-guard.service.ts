import {Injectable} from '@angular/core';
import {Authorization} from './authorization';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {

  constructor(public authorization: Authorization) {
  }

  canActivate(): boolean {
    this.authorization.isAuthenticated().subscribe(data => {
      return data;
    });
    return false;
  }

  isTechinician() {
    //return this.authorization.isTechnician;
  }
}
