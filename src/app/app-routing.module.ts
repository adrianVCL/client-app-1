import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
    {path: '', redirectTo: 'technician', pathMatch: 'full'},
    {path: 'technician', loadChildren: './technician/technician.module#TechnicianPageModule'},
    {path: 'login', loadChildren: './pages/login/login.module#LoginPageModule'},


];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [RouterModule],
    providers: [
    ]
})
export class AppRoutingModule {
}
