import {ErrorHandler, NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import {Api, AuthGuardService, Authorization, Base64ToBlob, SessionProvider, Tickets} from '../providers';
import { Network } from '@ionic-native/network/ngx';
import { Dialogs } from '@ionic-native/dialogs/ngx';
import { RefreshTokenInterceptor } from 'src/interceptors/refresh-token.interceptor';
import {Diagnostic} from '@ionic-native/diagnostic/ngx';
import { Deploy } from 'cordova-plugin-ionic/dist/ngx';
import {BugsnagErrorHandler} from '@bugsnag/plugin-angular';
import bugsnag from '@bugsnag/js';

//const bugsnagClient = bugsnag('2a0b061869a2c919ec141566855cc9d2');

// create a factory which will return the bugsnag error handler
/*export function errorHandlerFactory() {
    return new BugsnagErrorHandler(bugsnagClient);
}*/



// @ts-ignore
@NgModule({
    declarations: [AppComponent],
    entryComponents: [],
    imports: [
        BrowserModule,
        IonicModule.forRoot(),
        AppRoutingModule,
        HttpClientModule,
        IonicStorageModule.forRoot()
    ],
    providers: [
        StatusBar,
        SplashScreen,
        { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
        Api,
        Authorization,
        AuthGuardService,
        SessionProvider,
        Network,
        Dialogs,
        { provide: HTTP_INTERCEPTORS, useClass: RefreshTokenInterceptor, multi: true },
        Tickets,
        Diagnostic,
        Base64ToBlob,
        Deploy,
        //{ provide: ErrorHandler, useFactory: errorHandlerFactory }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
