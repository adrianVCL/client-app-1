import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Storage} from '@ionic/storage';
import {ModalController, Platform} from '@ionic/angular';
import {Dialogs} from '@ionic-native/dialogs/ngx';
import {Loading} from '../../../providers';
import {CloseTicketPage} from '../close-ticket/close-ticket.page';

@Component({
    selector: 'app-current-job',
    templateUrl: './current-job.page.html',
    styleUrls: ['./current-job.page.scss'],
})
export class CurrentJobPage implements OnInit {
    assignation = {};

    constructor(private router: Router, private route: ActivatedRoute, private storage: Storage, private dialogs: Dialogs,
                private loadingService: Loading, private platform: Platform, private modalController: ModalController) {
    }

    ngOnInit() {

    }

    closeTicket() {
        //this.router.navigate(['/technician/close-ticket']);
        this.dialogs.confirm('¿Esta seguro que desea cerrar el Ticket?', 'Cerrar Ticket', ['Si', 'Cancelar'])
            .then((confirm) => {
                if (confirm === 1) {
                    this.closeTicketModal();
                }
            })
            .catch(e => console.log('Error displaying dialog', e));
    }

    cancelJob() {
        this.dialogs.confirm('¿Esta seguro que desea cancelar el trabajo en el Ticket?', 'Cancelar Trabajo', ['Si', 'Cancelar'])
            .then((confirm) => {
                if (confirm === 1) {
                    this.storage.remove('assignation');
                    this.router.navigate(['technician/tickets']);
                }
            })
            .catch(e => console.log('Error displaying dialog', e));
    }

    async closeTicketModal() {
        const modal = await this.modalController.create({
            component: CloseTicketPage,
        });
        return await modal.present();
    }

    openMap(assignation) {
        if (assignation.ticket.client.coordinate) {
            const coordinates = JSON.parse(assignation.ticket.client.coordinate);
            console.log(coordinates);
            const destination = coordinates.latitude + ',' + coordinates.longitude;

            if (this.platform.is('ios')) {
                window.open('maps://?q=' + destination, '_system');
            } else {
                window.open('geo:0,0?q=' + destination, '_system');
            }
        } else {
            this.dialogs.alert('No existe ubicación para este cliente', 'Sin Ubicación', 'Ok');
        }

    }


    ionViewWillEnter() {
        this.loadingService.present();
        this.storage.get('assignation').then((data) => {
            if (data !== null && data !== undefined) {
                this.assignation = data;
            } else {
                this.dialogs.alert('No se encontro ticket abierto', 'Nota', 'Ok').then(() => {
                    this.router.navigate(['technician/tickets']);
                });
            }
            setTimeout(() => {
                this.loadingService.dismiss();
            }, 2000);
        });
    }

}
