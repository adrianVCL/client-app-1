import { ShowErrorLogPage } from './../show-error-log/show-error-log.page';
import { ModalController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-info-log',
  templateUrl: './info-log.page.html',
  styleUrls: ['./info-log.page.scss'],
})
export class InfoLogPage implements OnInit {

  errors = [];
  constructor(private storage: Storage, private modalController: ModalController) { }

  ngOnInit() { }

  ionViewWillEnter() {
    this.storage.get('logs-info').then((data) => {
      if (data) {
        console.log(data);
      }
    });

    this.storage.get('logs-error').then((data) => {
      if (data) {
        this.errors = data;
      }
    });
  }

  async showError(error) {
    const modal = await this.modalController.create({
        component: ShowErrorLogPage,
        componentProps: {
            errorData: error
        }
    });

    modal.onWillDismiss().then((data) => {});

    return await modal.present();
}

}
