import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';

import {IonicModule} from '@ionic/angular';

import {CompletedPage} from './completed.page';
import {Api, Loading, Tickets} from '../../../providers';
import {ShowTicketPage} from '../show-ticket/show-ticket.page';

const routes: Routes = [
    {
        path: '',
        component: CompletedPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes)
    ],
    declarations: [CompletedPage, ShowTicketPage],
    providers: [
        Api,
        Tickets,
        Loading
    ],
    entryComponents: [ShowTicketPage]
})
export class CompletedPageModule {
}
