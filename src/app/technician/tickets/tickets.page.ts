import {Component, OnInit} from '@angular/core';
import {Tickets} from '../../../providers';
import {Dialogs} from '@ionic-native/dialogs/ngx';
import {Router} from '@angular/router';
import {Loading} from '../../../providers';
import {Storage} from '@ionic/storage';

@Component({
    selector: 'app-tickets',
    templateUrl: './tickets.page.html',
    styleUrls: ['./tickets.page.scss'],
})
export class TicketsPage implements OnInit {
    assignations = [];

    constructor(private ticketsService: Tickets, private dialogs: Dialogs, public router: Router,
                private loadingService: Loading, private storage: Storage) {
    }

    ngOnInit() {
        /*
                this.ticketsService.index().subscribe((data: any) => {
                    if (!data.errorMessage) {
                        this.assignations = data.response.filter(items => {
                            return items.ticket.status === 'assigned';
                        });
                        this.storage.set('assignations', data.response.filter(items => items.ticket.status === 'assigned'));
                    } else {
                        this.storage.get('assignations').then((assignations) => {
                            if (assignations) {
                                this.assignations = assignations.filter(items => items.ticket.status === 'assigned');
                            }
                        });
                    }
                    this.loadingService.dismiss();
                });*/
    }

    openTicket(ticket, indexElement) {
        this.storage.set('index', indexElement);
        this.storage.get('assignation').then((data) => {
            if (data === null || data === undefined) {
                this.storage.set('assignation', ticket).then(() => {
                    this.dialogs.confirm('¿Esta seguro que desea comenzar el trabajo del ticket?', 'Inicar Ticket', ['Si', 'Cancelar'])
                        .then((confirm) => {
                            if (confirm === 1) {
                                this.router.navigate(['/technician/current-job']);
                            }
                        })
                        .catch(e => console.log('Error displaying dialog', e));
                });

            } else {
                this.dialogs.alert('Cuenta con una asignación sin cerrar, dirigase a la sección' +
                    'Ticket Actual para cerrarlo y poder abrir otro', 'Nota', 'Aceptar');
            }
        });
    }

    ionViewWillEnter() {
        this.refreshTickets(null);
    }

    refreshTickets($event) {
        this.loadingService.present();
        this.ticketsService.index({
            status: 'assigned'
        }).subscribe((data: any) => {
            if (!data.errorMessage && data.success) {
                this.assignations = data.response.filter(items => {
                    return items.ticket.status === 'assigned';
                });
                this.storage.set('assignations', data.response);
            } else {
                this.storage.get('assignations').then((assignations) => {
                    if (assignations) {
                        this.assignations = assignations.filter(items => items.ticket.status === 'assigned');
                    }
                });
            }
            setTimeout(() => {
                this.loadingService.dismiss();
                if ($event !== null) {
                    $event.target.complete();
                }
            }, 2000);
        });
    }

}
