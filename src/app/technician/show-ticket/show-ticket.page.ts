import {Component, Input, OnInit} from '@angular/core';
import {ModalController, NavParams} from '@ionic/angular';

@Component({
    selector: 'app-show-ticket',
    templateUrl: './show-ticket.page.html',
    styleUrls: ['./show-ticket.page.scss'],
})
export class ShowTicketPage implements OnInit {
    assignation;

    constructor(private navParams: NavParams, private modalController: ModalController) {
        this.assignation = navParams.get('ticket');
    }

    ngOnInit() {
    }

    closeModal() {
        this.modalController.dismiss();
    }

}
