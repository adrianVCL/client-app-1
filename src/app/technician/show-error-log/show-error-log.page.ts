import { ModalController, NavParams } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-show-error-log',
  templateUrl: './show-error-log.page.html',
  styleUrls: ['./show-error-log.page.scss'],
})
export class ShowErrorLogPage implements OnInit {

  error;
  constructor(private modalController: ModalController, private navParams: NavParams) {
    this.error = this.navParams.get('errorData');
    console.log(this.error);
  }

  ngOnInit() {
  }

  closeModal() {
    this.modalController.dismiss({});
  }

}
