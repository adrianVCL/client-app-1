import {Component, OnInit} from '@angular/core';
import {Loading} from '../../../providers';
import {Storage} from '@ionic/storage';
import {ModalController} from '@ionic/angular';
import {CloseTicketPage} from '../close-ticket/close-ticket.page';
import {EditTicketPage} from '../edit-ticket/edit-ticket.page';

@Component({
    selector: 'app-not-completed',
    templateUrl: './not-completed.page.html',
    styleUrls: ['./not-completed.page.scss'],
})
export class NotCompletedPage implements OnInit {
    failureJobs = [];

    constructor(private loadingService: Loading, private storage: Storage, private modalController: ModalController) {
    }

    ngOnInit() {
    }

    ionViewWillEnter() {
        this.refreshTickets(null);
    }

    refreshTickets($event = null) {
        this.loadingService.present();
        this.storage.get('completed-failures').then((data) => {
            if (data) {
                this.failureJobs = data;
            }
            setTimeout(() => {
                if ($event !== null) {
                    $event.target.complete();
                }
                this.loadingService.dismiss();
            }, 1500);
        });
    }

    async checkTicket(completed) {
        const modal = await this.modalController.create({
            component: EditTicketPage,
            componentProps: {
                completion: completed.completion
            }
        });

        modal.onWillDismiss().then((data) => {
            this.refreshTickets();
        });

        return await modal.present();
    }

}
